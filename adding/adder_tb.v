`include "adder.v"

module adder_tb;

    reg [3:0] A;
    reg [3:0] B;
    wire [7:0] Sum;

    adder uut (
        .A(A),
        .B(B),
        .Sum(Sum)
    );

    initial begin

        $dumpfile("adder_tb.vcd");
        $dumpvars(0, adder_tb);

        A = 4'b0001; B = 4'b0000; #10;
        $display("A = %d, B = %d, Sum = %d", A, B, Sum);

        A = 4'b0101; B = 4'b0011; #10;
        $display("A = %d, B = %d, Sum = %d", A, B, Sum);

        A = 4'b1111; B = 4'b0001; #10;
        $display("A = %d, B = %d, Sum = %d", A, B, Sum);

        A = 4'b1010; B = 4'b1010; #10;
        $display("A = %d, B = %d, Sum = %d", A, B, Sum);
    end

endmodule
