module parallelization;
  
  // Declare inputs and outputs
  input logic [7:0] vector_a;
  input logic [7:0] vector_b;
  output logic [15:0] result;

  // Declare variables
  logic [3:0] a_segment_0, a_segment_1;
  logic [3:0] b_segment_0, b_segment_1;
  logic [7:0] partial_result_0, partial_result_1;

  fork
    // Parallel block 1
    begin : parallel_block_1
      a_segment_0 = vector_a[3:0];
      b_segment_0 = vector_b[3:0];
      partial_result_0 = a_segment_0 * b_segment_0;
    end

    // Parallel block 2
    begin : parallel_block_2
      a_segment_1 = vector_a[7:4];
      b_segment_1 = vector_b[7:4];
      partial_result_1 = a_segment_1 * b_segment_1;
    end
  join
  
  // Combine partial results into the final result
  always_comb begin
    result = {partial_result_1, partial_result_0};
  end

endmodule