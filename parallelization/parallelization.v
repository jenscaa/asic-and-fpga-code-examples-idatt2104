module parallelization (
    input logic [7:0] vector_a,
    input logic [7:0] vector_b,
    output logic [15:0] result
);

  logic [3:0] a_segment_0, a_segment_1;
  logic [3:0] b_segment_0, b_segment_1;
  logic [7:0] partial_result_0, partial_result_1;

  always @* begin
    a_segment_0 = vector_a[3:0];
    b_segment_0 = vector_b[3:0];
    partial_result_0 = a_segment_0 * b_segment_0;
  end

  always @* begin
    a_segment_1 = vector_a[7:4];
    b_segment_1 = vector_b[7:4];
    partial_result_1 = a_segment_1 * b_segment_1;
  end

  always @* begin
    result = {partial_result_1, partial_result_0};
  end

endmodule
