`include "parallelization.sv"

module parallelization_tb;

    reg [7:0] vector_a;
    reg [7:0] vector_b;
    wire [15:0] result;

    parallelization dut (
        .vector_a(vector_a),
        .vector_b(vector_b),
        .result(result)
    );

    initial begin

        $dumpfile("parallelization.vcd");
        $dumpvars(0, parallelization_tb);

        vector_a = 8'b00000000;
        vector_b = 8'b00000000;
        #10;
        $display("\n[%d, %d ] * [%d, %d ] -> [%d, %d ]", 
        vector_a[7:4], vector_a[3:0], vector_b[7:4], 
        vector_b[3:0], result[15:8], result[7:0]);

        vector_a = 8'b00100001;
        vector_b = 8'b00100010;
        #10;
        $display("\n[%d, %d ] * [%d, %d ] -> [%d, %d ]", 
        vector_a[7:4], vector_a[3:0], vector_b[7:4], 
        vector_b[3:0], result[15:8], result[7:0]);

        vector_a = 8'b01100101;
        vector_b = 8'b10100011;
        #10;
        $display("\n[%d, %d ] * [%d, %d ] -> [%d, %d ]", 
        vector_a[7:4], vector_a[3:0], vector_b[7:4], 
        vector_b[3:0], result[15:8], result[7:0]);

        vector_a = 8'b01110101;
        vector_b = 8'b00101010;
        #10;
        $display("\n[%d, %d ] * [%d, %d ] -> [%d, %d ]\n", 
        vector_a[7:4], vector_a[3:0], vector_b[7:4], 
        vector_b[3:0], result[15:8], result[7:0]);

    end

endmodule
