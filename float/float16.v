module float16 (
    input wire [15:0] input_data,
    output sign,
    output [4:0] exponent,
    output [10:0] mantissa
);

    assign sign = input_data[15];
    assign exponent = input_data[14:10];
    assign mantissa = input_data[9:0];

endmodule