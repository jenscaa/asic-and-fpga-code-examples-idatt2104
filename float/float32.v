module float32 (
    input wire [31:0] input_data,
    output sign,
    output [7:0] exponent,
    output [22:0] mantissa
);

    assign sign = input_data[31];
    assign exponent = input_data[30:23];
    assign mantissa = input_data[22:0];

endmodule

// https://www.geeksforgeeks.org/ieee-standard-754-floating-point-numbers/