module float8 (
    input wire [7:0] input_data,
    output sign,
    output [3:0] exponent,
    output [3:0] mantissa
);

    assign sign = input_data[7];
    assign exponent = input_data[6:3];
    assign mantissa = input_data[2:0];

endmodule