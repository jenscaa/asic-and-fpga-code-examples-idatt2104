# ASIC and FPGA code examples IDATT2104

## Introduction
This repo is an additional submission for the voluntary project in IDATT2104 Nettverksprogrammering Vår 2024. These examples will show how programming in ASIC and FPGA works by using the HDL (Hardware Descriptive Language) Verilog.

## Getting started
In order to start and run these code examples you have to download the HDL simulator
Icarus verilog and the VS code extension Verilog HDL.
- [Icarus verilog](https://bleyer.org/icarus/)
- [Verilog HDL](https://marketplace.visualstudio.com/items?itemName=leafvmaple.verilog)

To ensure that Icarus verilog was install succesfully you can type in the terminal
```bash
iverilog
```
and if no error occured, then you are good. Make sure to check the "Add executable folders to user PATH" checkbox. If troubling to install Icarus Verilog see this tutorial for more info:
- [How to simulate verilog files using iverilog and GTKWave](https://www.youtube.com/watch?v=FqIhFxf9kFM)

Now in order to compile and run these verilog example codes, it should appear a green button at the top right corner of the VS code editor.