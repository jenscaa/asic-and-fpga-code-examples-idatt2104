module elementwise_multiplication (
    input [7:0] vector_a,
    input [7:0] vector_b,
    output [15:0] result
);

integer i = 0;
reg [3:0] a_segment, b_segment;
reg [7:0] partial_result;
reg [15:0] temp_result;

always @* begin
    a_segment = 4'b0;
    b_segment = 4'b0;
    partial_result = 8'b0;
    temp_result = 16'b0;

    for (i = 0; i < 2; i = i + 1) begin
        a_segment = vector_a[7 - i*4 -: 4];
        b_segment = vector_b[7 - i*4 -: 4];
        partial_result = a_segment * b_segment;
        temp_result = (temp_result << (i*8)) + partial_result;
    end
end

assign result = temp_result;

endmodule
